package com.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.http.HttpServlet;

public class ViewServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    static String centralUtilFile = File.separatorChar + "centralUtil.properties";
    
	public void init() {		
		Timer timer = new Timer();
		Properties properties = new Properties();
		InputStream input = null;
		try {
			File tempFile = new File(getPath() + centralUtilFile);
			boolean exists = tempFile.exists();
			System.out.println(exists);
			if(exists == false) {
				createPropertyFile();
			}
			String tomcatBase = System.getenv("ProgramFiles");
			System.out.println("Path : " + tomcatBase);
			input = new FileInputStream(getPath() + centralUtilFile);
			properties.load(input);
			String sleep = properties.getProperty("timerThread");
			String scheduler = properties.getProperty("timerScheduler");
			System.out.println("Util File Found" + getPath());
			TimerTask delayedThreadStartTask = new TimerTask() {
				@Override
				public void run() {
					Runnable readTask = () -> {
						while (true) {
							mainServlet.test(properties);
							try {
								Thread.sleep(Long.parseUnsignedLong(sleep));
							} catch (NumberFormatException e) {
								System.out.println("Number Format is wrong");
								e.printStackTrace();
							} catch (InterruptedException e) {
								System.out.println("Thread stopped");
								e.printStackTrace();
							} 

						}

					};
					readTask.run();
				}

			};
			timer.schedule(delayedThreadStartTask, Long.parseUnsignedLong(scheduler));
		} catch (FileNotFoundException e1) {
			System.out.println("Util File Not Found");
			e1.printStackTrace();
		} catch (IOException e1) {
			System.out.println("Util File Not Found");
			e1.printStackTrace();
		}
	}

	public String getPath() {
		String fullPath = "";
		try {
			String path = this.getClass().getClassLoader().getResource("").getPath();
			fullPath = URLDecoder.decode(path, "UTF-8");
			String warName = new File(getServletContext().getRealPath("/")).getName();
			String pathArr[] = fullPath.split("/" + warName + "/WEB-INF/classes/");
			System.out.println(fullPath);
			System.out.println(pathArr[0]);
			fullPath = pathArr[0];	
		} catch (UnsupportedEncodingException e1) {
			System.out.println("Encoding of path not supported");
			e1.getStackTrace();
		}
		return fullPath;
	}
	
	public String createPropertyFile() {
		try {
	      Properties props = new Properties();
	      props.put("URL", "com.mysql.jdbc.Driver");
	      props.put("refreshtime", "1");
	      props.put("dbURL", "jdbc:mysql://localhost:3306/");
	      props.put("dbName", "wwwindia_eh4u_Master");
	      props.put("dbUsername", "root");
	      props.put("dbPassword", "mysql");
	      props.put("sqlQuery", "select count(s_id),sum(v_gross_amt),max(v_bill_no),v_date from t_bill where v_date between '2021-05-20' and curdate() group by v_date;");
	      props.put("internetUrl", "http://www.google.com");
	      props.put("updateUrl", "http://164.52.194.61:8081/albaikcentral/sales/update");
	      props.put("lastDateUrl", "http://164.52.194.61:8081/albaikcentral/sales/last/");
	      props.put("storeId", "12");
	      props.put("timerThread", "1800000");
	      props.put("timerScheduler", "1800000");
	      String path = getPath() + centralUtilFile;
	      FileOutputStream outputStrem = new FileOutputStream(path);
	      props.store(outputStrem, "This is a sample properties file");
	      System.out.println("Properties file created......");
		}catch(IOException e) {
			 System.out.println("Properties file creating issue......");
			e.getStackTrace();
		}
	      return "Properties file created......";
	}

}


