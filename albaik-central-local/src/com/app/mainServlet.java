package com.app;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;
import org.json.simple.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;

public class mainServlet {
	public static void test(Properties properties) {
		try {
			URL url = new URL(properties.getProperty("internetUrl"));
			URLConnection connection = url.openConnection();
			connection.connect();
			System.out.println("Connection checked");
			Client client = ClientBuilder.newClient();
			WebTarget myResource = client
					.target(properties.getProperty("lastDateUrl") + (properties.getProperty("storeId")));
			String resp = myResource.request(MediaType.TEXT_PLAIN).get(String.class);
			String query = properties.getProperty("sqlQuery");
			String replaceString = query.replace("${dynamicDate}", resp);
			System.out.println(replaceString);
			Class.forName(properties.getProperty("URL"));
			Connection con = DriverManager.getConnection(
					properties.getProperty("dbURL") + properties.getProperty("dbName"),
					properties.getProperty("dbUsername"), properties.getProperty("dbPassword"));
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(replaceString);
			System.out.println(con);
			while (rs.next()) {
				JSONObject saleObj = new JSONObject();
				saleObj.put("totalBills", rs.getString(1));
				saleObj.put("totalSale", rs.getString(2));
				saleObj.put("lastBillNo", rs.getString(3));
				saleObj.put("date", rs.getString(4));
				JSONObject storeObj = new JSONObject();
				storeObj.put("id", properties.getProperty("storeId"));
				saleObj.put("storeId", storeObj);
				ObjectMapper Obj = new ObjectMapper();
				String jsonStr = Obj.writeValueAsString(saleObj);
				ClientConfig config = new ClientConfig();
				Client client1 = ClientBuilder.newClient(config);
				WebTarget target = client1.target(properties.getProperty("updateUrl"));
				String sale1 = target.request().accept(MediaType.APPLICATION_JSON).put(Entity.json(jsonStr),
						String.class);
			}
			System.out.println("Running");
			con.close();
		} catch (Exception e) {
			System.out.println("Internet or Server is not Connected");
		}
	}


}
