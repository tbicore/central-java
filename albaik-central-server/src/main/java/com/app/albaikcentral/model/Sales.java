package com.app.albaikcentral.model;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Sales {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String date;
	private String totalSale;
	private String updatedOn;
	private String totalBills;
	private String lastBillNo;
	
	 @ManyToOne(cascade=CascadeType.MERGE,fetch = FetchType.EAGER)
	    @JoinColumn(name = "storeId", referencedColumnName="id")
	    private Store storeId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTotalSale() {
		return totalSale;
	}
	public void setTotalSale(String totalSale) {
		this.totalSale = totalSale;
	}
	public String getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(String updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Store getStoreId() {
		return storeId;
	}
	public void setStoreId(Store storeId) {
		this.storeId = storeId;
	}
	public String getTotalBills() {
		return totalBills;
	}
	public void setTotalBills(String totalBills) {
		this.totalBills = totalBills;
	}
	public String getLastBillNo() {
		return lastBillNo;
	}
	public void setLastBillNo(String lastBillNo) {
		this.lastBillNo = lastBillNo;
	}
	
	
}
