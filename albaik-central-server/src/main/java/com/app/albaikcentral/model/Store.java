package com.app.albaikcentral.model;


import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
@Entity
public class Store {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private String owner;
	private String city;
	private String state;
	private String type;
	private String ownerContactNumber;
	private String status;
	private String lastSyncDateTime;
	
	
	/*
	 * @ManyToOne(cascade=CascadeType.MERGE,fetch = FetchType.EAGER)
	 * 
	 * @JoinColumn(name = "clientId", referencedColumnName="id") private Client
	 * client;
	 */
	 
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOwnerContactNumber() {
		return ownerContactNumber;
	}

	public void setOwnerContactNumber(String ownerContactNumber) {
		this.ownerContactNumber = ownerContactNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLastSyncDateTime() {
		return lastSyncDateTime;
	}

	public void setLastSyncDateTime(String lastSyncDateTime) {
		this.lastSyncDateTime = lastSyncDateTime;
	}
	
	/*
	 * public Client getClient() { return client; }
	 * 
	 * public void setClient(Client client) { this.client = client; }
	 */
	 

	

	
	

}
