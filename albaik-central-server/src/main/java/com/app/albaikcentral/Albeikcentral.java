package com.app.albaikcentral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication 
public class Albeikcentral extends SpringBootServletInitializer{
	public static void main(String[] args)  
	{    
	SpringApplication.run(Albeikcentral.class, args);    
	}   
	 @Override
	    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
	        return builder.sources(Albeikcentral.class);
	    }
}
