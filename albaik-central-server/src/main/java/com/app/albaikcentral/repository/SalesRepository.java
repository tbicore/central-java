package com.app.albaikcentral.repository;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.app.albaikcentral.model.Sales;
import com.app.albaikcentral.model.Store;

public interface SalesRepository extends JpaRepository<Sales, Integer>{
	public List<Sales> findByDate(String date);
	
	public Sales findTopByStoreIdOrderByDateDesc(Store storeId);
		
	@Query(nativeQuery = true, value="select * from sales s where s.date between :startDate and :endDate and s.store_id = :storeId")
	List<Sales> getData_between(@Param("startDate") String date, @Param("endDate") String date2,@Param("storeId") Store storeId);
	
	@Query(nativeQuery = true, value="select * from sales s where s.date between :startDate and :endDate")
	List<Sales> getData_betweenDate(@Param("startDate") String date, @Param("endDate") String date2);
	
}
