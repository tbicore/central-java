package com.app.albaikcentral.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.albaikcentral.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	public User findByUserName(String userName);

	

}
