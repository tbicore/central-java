package com.app.albaikcentral.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.app.albaikcentral.model.Store;

public interface StoreRepository extends JpaRepository<Store, Integer>{

}
