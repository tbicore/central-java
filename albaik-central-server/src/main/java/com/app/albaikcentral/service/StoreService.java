package com.app.albaikcentral.service;

import java.util.List;

import com.app.albaikcentral.model.Store;

public interface StoreService {

	public Store saveStore(Store store);

	public List<Store> getAll();

	public void delete(Integer id);

	public Store findById(Integer id);

	public void updateStore(Store store);

}
