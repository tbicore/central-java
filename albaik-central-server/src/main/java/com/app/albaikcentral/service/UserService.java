package com.app.albaikcentral.service;

import java.util.List;

import com.app.albaikcentral.model.User;

public interface UserService {
	public User saveUser(User user);

	public List<User> getAll();
	
	public void delete(Integer id);

	public User findById(Integer id);
	
	public User findByUserName(String userName);

	public void updateUser(User user);
}
