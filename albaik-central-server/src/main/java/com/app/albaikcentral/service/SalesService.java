package com.app.albaikcentral.service;
import java.util.List;

import org.springframework.data.repository.query.Param;

import com.app.albaikcentral.model.Sales;
import com.app.albaikcentral.model.Store;

public interface SalesService {
	
	public void delete(Integer id);
	
	public void updateSales(Sales sale);
	
	public List<Sales> findByDate(String date);
	
	public Sales saveSales(Sales sales);
	
	public Sales findTopByStoreIdOrderByDateDesc(Store storeId);
	
	List<Sales> getData_between(@Param("startDate") String date, @Param("endDate") String date2,@Param("storeId") Store storeId);
	
	List<Sales> getData_betweenDate(@Param("startDate") String date, @Param("endDate") String date2);

}
