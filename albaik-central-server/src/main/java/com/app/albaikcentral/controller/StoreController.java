package com.app.albaikcentral.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.albaikcentral.model.Store;
import com.app.albaikcentral.service.StoreService;

@RestController
@CrossOrigin
@RequestMapping("/store")
public class StoreController {
	@Autowired
	private StoreService storeService;

	@PostMapping("/save")
	public Store saveStore(@RequestBody Store store) throws Exception {
		return storeService.saveStore(store);
	}
	
	@GetMapping("/")
	public List<Store> getAll(){
		return storeService.getAll();
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Store> deleteUser(@PathVariable(value = "id") Integer id) {
		storeService.delete(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{id}")
	public Store findById(@PathVariable Integer id) {
		return storeService.findById(id);
	}
	@PutMapping("/update")
    public String updateStore(@Valid @RequestBody Store  store) throws Exception {
		storeService.updateStore(store);
        return  "200 : Update Success";
   }
	

}
