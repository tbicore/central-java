package com.app.albaikcentral.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.app.albaikcentral.model.Store;
import com.app.albaikcentral.model.User;
import com.app.albaikcentral.service.UserService;
@EnableWebMvc
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
		
	@PostMapping("/save")
	public User saveUser(@RequestBody User user)throws Exception {
		return userService.saveUser(user);
	}
	
	
	 @DeleteMapping("/{id}")
		public ResponseEntity<Store> deleteUser(@PathVariable(value = "id") Integer id) {
		 userService.delete(id);
			return ResponseEntity.ok().build();
		}
		
		@GetMapping("/{id}")
		public User findById(@PathVariable Integer id) {
			return userService.findById(id);
		}
		@PutMapping("/update")
	    public String updateUser(@Valid @RequestBody User  user) throws Exception {
			userService.updateUser(user);
	        return  "200 : Update Success";
	   }
		

		@PostMapping("/login") 
		public ResponseEntity<String> login(@RequestBody User user) { 
			ResponseEntity<String> resp = null; 
			User userExists =userService.findByUserName(user.getUserName());
		try {
			if (userExists != null) {
				
			 if(userExists.getUserName().equals(user.getUserName())) {
				
			 if(userExists.getPassword().equals(user.getPassword())) {
					resp = new ResponseEntity<String>("User login Successfully",HttpStatus.OK);
				}
				 else { 
					resp = new ResponseEntity<String>("Password  Not Found ",HttpStatus.BAD_REQUEST);
				}
			}
		}else { 
			resp = new ResponseEntity<String>("User Name Not Found ",HttpStatus.BAD_REQUEST);
		}
		} catch (Exception e) {
			resp = new ResponseEntity<String>("Unable to Get User Name", HttpStatus.INTERNAL_SERVER_ERROR);
			e.printStackTrace();
		}
		return resp;
	
		}	 
}
