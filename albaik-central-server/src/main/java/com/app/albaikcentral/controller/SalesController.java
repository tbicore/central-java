package com.app.albaikcentral.controller;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.albaikcentral.model.Sales;
import com.app.albaikcentral.model.Store;
import com.app.albaikcentral.service.SalesService;

@RestController
@CrossOrigin
@RequestMapping("/sales")
public class SalesController {
	
	@Autowired
	private SalesService salesService;
	
	@PostMapping("/save")
	public Sales saveSales(@RequestBody Sales sales) throws Exception {
		return salesService.saveSales(sales);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Sales> deleteUser(@PathVariable(value = "id") Integer id) {
		salesService.delete(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/fetch/{storeId}/{one_date}/{two_date}")
	public List<Sales> getData_between(@PathVariable(value = "storeId") Store storeId, @PathVariable(value = "one_date") @DateTimeFormat(pattern = "yyyy-MM-dd") String fromDate, @PathVariable(value = "two_date") @DateTimeFormat(pattern = "yyyy-MM-dd") String toDate) {
		if(storeId==null) {
		   return salesService.getData_betweenDate(fromDate, toDate);   
	   }
		return salesService.getData_between(fromDate, toDate,storeId);
	}

	
	@PutMapping("/update")
	public Sales updateSales(@Valid @RequestBody Sales sale) throws Exception {
		//DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		List<Sales> sales = salesService.findByDate(sale.getDate());
		if (!sales.isEmpty() && sales !=null) {
			List<Sales> listOutput =
					sales.stream()
				            .filter(e -> e.getStoreId().getId().equals(sale.getStoreId().getId()))
				            .collect(Collectors.toList());
			System.out.println(listOutput);
			if(!listOutput.equals(null) && !listOutput.isEmpty()) {
				for(Sales existSale : listOutput) {
					if(existSale.getStoreId().getId()==sale.getStoreId().getId()) {
						existSale.setTotalSale(sale.getTotalSale());
						existSale.setLastBillNo(sale.getLastBillNo());
						existSale.setTotalBills(sale.getTotalBills());
						salesService.updateSales(existSale);
					}else {
						salesService.saveSales(sale);
					}	
				}
			}else {
				salesService.saveSales(sale);	
			}
		}else{
			salesService.saveSales(sale);
		}
		return sale;
	}

	@GetMapping("/last/{storeId}")
	public String findTopByStoreIdOrderByDateDesc(@PathVariable(value = "storeId") Store storeId) {
	Sales sale = salesService.findTopByStoreIdOrderByDateDesc(storeId);
	String lastDate=null;
	if(sale!=null) {
		 lastDate = sale.getDate();
	}else {
		 lastDate="2021-01-01";
		}
		return lastDate;
	}

}
