/*
 * package com.app.albaikcentral.controller;
 * 
 * import java.util.List;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.web.bind.annotation.CrossOrigin; import
 * org.springframework.web.bind.annotation.GetMapping; import
 * org.springframework.web.bind.annotation.PostMapping; import
 * org.springframework.web.bind.annotation.RequestBody; import
 * org.springframework.web.bind.annotation.RequestMapping; import
 * org.springframework.web.bind.annotation.RestController;
 * 
 * import com.app.albaikcentral.model.Client; import
 * com.app.albaikcentral.model.Store; import
 * com.app.albaikcentral.service.ClientService;
 * 
 * @RestController
 * 
 * @CrossOrigin
 * 
 * @RequestMapping("/client") public class ClientController {
 * 
 * @Autowired private ClientService clientService;
 * 
 * @PostMapping("/save") public Client saveClient(@RequestBody Client client)
 * throws Exception { return clientService.saveClient(client); }
 * 
 * @GetMapping("/") public List<Client> getAll(){ return clientService.getAll();
 * }
 * 
 * 
 * 
 * }
 */