/*
 * package com.app.albaikcentral.serviceImpl;
 * 
 * import java.util.List;
 * 
 * import org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.stereotype.Service;
 * 
 * import com.app.albaikcentral.model.Client; import
 * com.app.albaikcentral.model.Store; import
 * com.app.albaikcentral.repository.ClientRepository; import
 * com.app.albaikcentral.service.ClientService;
 * 
 * @Service public class ClientServiceImpl implements ClientService {
 * 
 * @Autowired private ClientRepository clientRepository;
 * 
 * @Override public List<Client> getAll() { return clientRepository.findAll(); }
 * 
 * @Override public Client saveClient(Client client) { return
 * clientRepository.save(client); }
 * 
 * }
 */