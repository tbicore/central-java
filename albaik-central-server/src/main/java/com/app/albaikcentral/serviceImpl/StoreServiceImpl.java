package com.app.albaikcentral.serviceImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.albaikcentral.model.Store;
import com.app.albaikcentral.repository.StoreRepository;
import com.app.albaikcentral.service.StoreService;
@Service
public class StoreServiceImpl implements StoreService{
	@Autowired
	private StoreRepository storeRepository;

	@Override
	public Store saveStore(Store store) {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss"); 
		 LocalDateTime now = LocalDateTime.now();  
		store.setLastSyncDateTime(dtf.format(now));
		return storeRepository.save(store);
	}

	@Override
	public List<Store> getAll() {
		return storeRepository.findAll();
	}

	@Override
	public void delete(Integer id) {
		Store store=storeRepository.findOne(id);
		storeRepository.delete(store);
	}

	

	@Override
	public Store findById(Integer id) {
		return storeRepository.findOne(id);
	}

	@Override
	public void updateStore(Store store) {
		storeRepository.save(store);
	}

}
