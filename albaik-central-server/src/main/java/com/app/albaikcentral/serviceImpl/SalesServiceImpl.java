package com.app.albaikcentral.serviceImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.app.albaikcentral.model.Sales;
import com.app.albaikcentral.model.Store;
import com.app.albaikcentral.repository.SalesRepository;
import com.app.albaikcentral.service.SalesService;

@Service
public class SalesServiceImpl implements SalesService{
@Autowired
private SalesRepository salesRepository;

	@Override
	public void delete(Integer id) {
		salesRepository.delete(id);
	}

	@Override
	public void updateSales(Sales sales) {
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now();  
		   sales.setUpdatedOn(dtf.format(now));
		salesRepository.save(sales);
	}

	@Override
	public List<Sales> findByDate(String date) {
		return salesRepository.findByDate(date);
	}

	@Override
	public Sales saveSales(Sales sales) {
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");  
		   LocalDateTime now = LocalDateTime.now();  
		   sales.setUpdatedOn(dtf.format(now));
		 
		return salesRepository.save(sales);
	}

	@Override
	public Sales findTopByStoreIdOrderByDateDesc(Store storeId) {
		return salesRepository.findTopByStoreIdOrderByDateDesc(storeId);		
	}

	@Override
	public List<Sales> getData_between(@Param("startDate") String date, @Param("endDate") String date2,@Param("storeId") Store storeId) {
		return salesRepository.getData_between(date, date2, storeId);
	}

	@Override
	public List<Sales> getData_betweenDate(String date, String date2) {
		return salesRepository.getData_betweenDate(date, date2);
	}
	
}
