package com.app.albaikcentral.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.app.albaikcentral.model.User;
import com.app.albaikcentral.repository.UserRepository;
import com.app.albaikcentral.service.UserService;
@Service
public class UserServiceImpl implements UserService {
@Autowired
private UserRepository userRepository;
	@Override
	public User saveUser(User user) {
		return userRepository.save(user);
	}
	@Override
	public List<User> getAll() {
		return userRepository.findAll();
	}
	@Override
	public void delete(Integer id) {
		userRepository.delete(id);
	}
	@Override
	public User findById(Integer id) {
		return userRepository.findOne(id);
	}
	@Override
	public void updateUser(User user) {
		userRepository.save(user);
	}
	@Override
	public User findByUserName(String userName) {
		return userRepository.findByUserName(userName);
	}
	

}
